#[cfg(debug_assertions)]
macro_rules! cfg_debug {
    (@debug $($t:tt)*) => { $($t)* };
    (@release $($t:tt)*) => { };
}

#[cfg(not(debug_assertions))]
macro_rules! cfg_debug {
    (@debug $($t:tt)*) => { };
    (@release $($t:tt)*) => { $($t)* };
}

cfg_debug! { @debug
    mod debug;
    pub use debug::Tera;
}

cfg_debug! { @release
    mod release;
    pub use release::Tera;
}

use poem::{
    Request, RequestBody, FromRequest,
    Endpoint, Middleware,
    http::StatusCode
};

#[poem::async_trait]
impl<'a> FromRequest<'a> for Tera {
    async fn from_request(req: &'a Request, _body: &mut RequestBody) ->  poem::Result<Self> {
        req.data().cloned().ok_or_else(|| {
            // todo: make panic?
            tracing::error!("Missing `Tera` instance, have you applied the `Engine` middleware?");

            StatusCode::INTERNAL_SERVER_ERROR.into()
        })
    }
}

pub struct Engine {
    tera: Tera
}

impl Engine {
    pub fn new(template_directory: &str) -> tera::Result<Self> {
        Ok(Self {
            tera: Tera::new(template_directory)?
        })
    }
}

impl<E: Endpoint> Middleware<E> for Engine {
    type Output = Render<E>;

    fn transform(&self, ep: E) -> Self::Output {
        Render {
            ep,
            tera: self.tera.clone()
        }
    }
}

pub struct Render<E: Endpoint> {
    ep: E,
    tera: Tera,
}

#[poem::async_trait]
impl<E: Endpoint> Endpoint for Render<E> {
    type Output = E::Output;

    async fn call(&self, mut req: Request) -> poem::Result<Self::Output> {
        req.set_data(self.tera.clone());
        self.ep.call(req).await
    }
}
