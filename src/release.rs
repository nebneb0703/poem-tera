use poem::web::Html;

use tera::{ Context, Result };

use std::sync::Arc;

#[derive(Clone)]
pub struct Tera(Arc<tera::Tera>);

impl Tera {
    pub(crate) fn new(template_directory: &str) -> tera::Result<Self> {
        let mut tera = tera::Tera::new(&format!("{template_directory}/**/*"))?;
        tera.autoescape_on(vec![".html.tera", ".htm.tera", ".xml.tera", ".html", ".htm", ".xml"]);

        Ok(Self(Arc::new(tera)))
    }

    pub async fn render(&self, template: &str, context: &Context) -> Result<Html<String>> {
        let result = self.0.render(template, context);

        if let Err(e) = &result {
            tracing::error!("Failed to render template: {e}");
            tracing::debug!("Render templates error: {e:?}");
        }

        result.map(Html)
    }
}