use poem::web::Html;

use tera::{ Context, Result };

use notify::{ Watcher, RecursiveMode, Event, EventKind };

use tokio::sync::RwLock;

use std::sync::{
    atomic::{ AtomicBool, Ordering },
    Arc,
};

#[derive(Clone)]
pub struct Tera {
    renderer: Arc<RwLock<tera::Tera>>,
    needs_reload: Arc<AtomicBool>,
    _watcher: Option<Arc<dyn Watcher + Send + Sync + 'static>>,
}

impl Tera {
    pub(crate) fn new(template_directory: &str) -> Result<Self> {
        let mut tera = tera::Tera::new(&format!("{template_directory}/**/*"))?;
        tera.autoescape_on(vec![".html.tera", ".htm.tera", ".xml.tera", ".html", ".htm", ".xml"]);

        let needs_reload = Arc::new(AtomicBool::new(false));
        let needs_reload_cloned = needs_reload.clone();

        let watcher = notify::recommended_watcher(move |event| match event {
            Ok(Event {
                kind:
                    EventKind::Create(_)
                    | EventKind::Modify(_)
                    | EventKind::Remove(_),
                ..
            }) => {
                needs_reload.store(true, Ordering::Relaxed);
                tracing::debug!("Sent reload request");
            },
            Err(e) => {
                // Ignore errors for now and just output them.
                // todo: make panic?
                tracing::debug!("Watcher error: {e:?}");
            },
            _ => {},
        });

        let watcher = watcher
            .map(|mut w| w
                .watch(std::path::Path::new(template_directory), RecursiveMode::Recursive)
                .map(|_| w)
            );

        let watcher = match watcher {
            Ok(Ok(w)) => {
                tracing::info!("Watching templates directory `{template_directory}` for changes.");

                Some(Arc::new(w) as Arc<dyn Watcher + Send + Sync>)
            }
            Err(e) | Ok(Err(e)) => {
                tracing::error!("Failed to start watcher: {e}");
                tracing::debug!("Watcher error: {e:?}");

                None
            },
        };

        Ok(Self {
            renderer: Arc::new(RwLock::new(tera)),
            _watcher: watcher,
            needs_reload: needs_reload_cloned,
        })
    }

    #[tracing::instrument(skip(self))]
    pub async fn render(&self, template: &str, context: &Context) -> Result<Html<String>> {
        let needs_reload = self.needs_reload.swap(false, Ordering::Relaxed);

        let lock = if needs_reload {
            tracing::info!("Detected changes to templates, reloading...");

            let mut lock = self.renderer.write().await;

            if let Err(e) = lock.full_reload() {
                tracing::error!("Failed to reload templates: {e}");
                tracing::debug!("Reload templates error: {e:?}");

                return Err(e);
            }

            lock.downgrade()
        } else {
            self.renderer.read().await
        };

        let result = lock.render(template, context);

        if let Err(e) = &result {
            tracing::error!("Failed to render template: {e}");
            tracing::debug!("Render templates error: {e:?}");
        }

        result.map(Html)
    }
}